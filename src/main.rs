use game::Game;
use player::Player;

use crate::{magic_generator::gen_attack_mask_rook, position::Position};
mod bitboard;
mod board;
mod colour;
mod errors;
mod game;
mod magic_generator;
mod p_move;
mod piece;
mod piece_map;
mod player;
mod position;

#[tokio::main]
async fn main() {
    let mut game = Game::new(Player::Person, Player::Person);

    game.play();
}
