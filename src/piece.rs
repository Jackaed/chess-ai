use std::{fmt::Display, str::FromStr};

use enum_map::*;
use itertools::iproduct;
use strum::IntoEnumIterator;
use strum_macros::EnumIter;

use crate::{colour::Colour, errors::InvalidChar};

#[derive(
    Clone,
    Copy,
    PartialEq,
    enum_map::Enum,
    strum_macros::EnumString,
    strum_macros::Display,
    EnumIter,
)]
pub enum Kind {
    #[strum(serialize = "p")]
    Pawn,
    #[strum(serialize = "n")]
    Knight,
    #[strum(serialize = "b")]
    Bishop,
    #[strum(serialize = "r")]
    Rook,
    #[strum(serialize = "k")]
    King,
    #[strum(serialize = "q")]
    Queen,
}

impl TryFrom<char> for Kind {
    type Error = InvalidChar;
    fn try_from(character: char) -> Result<Self, Self::Error> {
        Kind::from_str(&character.to_ascii_lowercase().to_string()).map_err(|_| InvalidChar {})
    }
}

#[derive(Clone, Copy)]
pub struct Piece {
    pub kind: Kind,
    pub colour: Colour,
}

impl Piece {
    pub fn new(kind: Kind, colour: Colour) -> Piece {
        Piece { kind, colour }
    }

    pub fn iter() -> impl Iterator<Item = Self> {
        iproduct!(Colour::iter(), Kind::iter()).map(|(colour, kind)| Piece::new(kind, colour))
    }
}

impl TryFrom<char> for Piece {
    type Error = InvalidChar;
    fn try_from(character: char) -> Result<Self, InvalidChar> {
        if !character.is_ascii_alphabetic() {
            return Err(InvalidChar {});
        }

        let colour = if character.is_lowercase() {
            Colour::Black
        } else {
            Colour::White
        };
        let kind = Kind::try_from(character);
        if let Ok(kind) = kind {
            Ok(Piece::new(kind, colour))
        } else {
            Err(InvalidChar {})
        }
    }
}

impl Display for Piece {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", {
            let c = Kind::to_string(&self.kind);
            if self.colour == Colour::White {
                c.to_ascii_uppercase()
            } else {
                c.to_ascii_lowercase()
            }
        })
    }
}
