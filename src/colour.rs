use strum_macros::EnumIter;

use crate::errors::InvalidChar;

#[derive(PartialEq, enum_map::Enum, Clone, Copy, EnumIter)]
pub enum Colour {
    White,
    Black,
}

impl Colour {
    /// Gets the opposite colour to itself (inverts it), E.G. White becomes
    /// Black.
    pub fn other(&self) -> Self {
        match *self {
            Self::White => Self::Black,
            Self::Black => Self::White,
        }
    }
}

impl Into<char> for Colour {
    fn into(self) -> char {
        match self {
            Colour::White => 'w',
            Colour::Black => 'b',
        }
    }
}

impl TryFrom<char> for Colour {
    type Error = InvalidChar;
    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'w' => Ok(Colour::White),
            'b' => Ok(Colour::Black),
            _ => Err(InvalidChar {}),
        }
    }
}
