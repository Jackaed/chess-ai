/// ! THIS FILE IS DEPRECATED, AND NO LONGER USED. THIS CODE IS ONLY HERE FOR FUTURE USE!
use std::{fs, sync::mpsc};

use macroquad::prelude::*;
use strum::IntoEnumIterator;
use tokio::sync::watch;

use crate::{
    board::{Board, Square},
    colour::Colour,
    piece::Piece,
    piece_map::{self, PieceMap},
    position::{self, File, Position, Rank},
};

#[derive(Clone)]
pub enum MousePosOnBoard {
    MouseOffOfBoard,
    MouseOnBoard(Position),
}

pub struct MouseInformation {
    mouse_pos_on_board: MousePosOnBoard,
    left_mouse_pressed: bool,
    left_mouse_pressed_last_frame: bool,
}

impl MouseInformation {
    pub fn mouse_pos_on_board(&self) -> &MousePosOnBoard {
        &self.mouse_pos_on_board
    }

    pub fn mouse_depressed(&self) -> bool {
        self.left_mouse_pressed
    }

    pub fn left_mouse_pressed_last_frame(&self) -> bool {
        self.left_mouse_pressed_last_frame
    }
}

pub enum PlayerRequest {
    None,
    HighlightPiece(Position),
    AttachPieceToCursor(Position),
    UnHighlightAndDetach,
}

pub struct RenderConfig {
    background_colour: Color,
    white_squares_colour: Color,
    black_squares_colour: Color,
    font_dir: String,
}

impl RenderConfig {
    pub fn background_colour(&self) -> Color {
        self.background_colour
    }

    pub fn white_squares_colour(&self) -> Color {
        self.white_squares_colour
    }

    pub fn black_squares_colour(&self) -> Color {
        self.black_squares_colour
    }

    pub fn font_dir(&self) -> &str {
        self.font_dir.as_ref()
    }

    pub fn get_texture_path(&self, piece: &Piece) -> String {
        format!(
            "{}/{}{}.svg",
            self.font_dir(),
            if piece.colour == Colour::White {
                "w"
            } else {
                "b"
            },
            piece.kind.to_string().to_ascii_uppercase()
        )
    }

    pub fn get_svg_strings(&self) -> PieceMap<String> {
        piece_map::PieceMap::new(|piece| {
            fs::read_to_string(self.get_texture_path(&piece).as_str())
                .expect(format!("Texture not found at {}", self.font_dir()).as_str())
        })
    }
}

pub fn get_board_render_config() -> RenderConfig {
    RenderConfig {
        background_colour: Color::from_rgba(0, 0, 0, 255),
        white_squares_colour: Color::from_rgba(237, 185, 168, 255),
        black_squares_colour: Color::from_rgba(155, 75, 48, 255),
        font_dir: String::from("./font/cburnett"),
    }
}

pub struct Renderer {
    board_receiver: watch::Receiver<Board>,
    info_sender: watch::Sender<MouseInformation>,
    svg_strings: PieceMap<String>,
    config: RenderConfig,
    textures: PieceMap<Texture2D>,
    prev_square_size: f32,
    player_request_receiver: Option<mpsc::Receiver<PlayerRequest>>,
    left_mouse_pressed_last_frame: bool,
}

impl Renderer {
    fn generate_textures(svg_strings: &PieceMap<String>) -> PieceMap<Texture2D> {
        PieceMap::new(|piece| {
            Self::svg_to_texture(
                svg_strings.get(&piece),
                Self::get_square_size().round() as u32,
            )
        })
    }

    fn textures(&mut self) -> &PieceMap<Texture2D> {
        let square_size = Self::get_square_size();
        if self.prev_square_size != square_size {
            self.textures = Self::generate_textures(&self.svg_strings);
            self.prev_square_size = square_size;
        }
        &self.textures
    }

    pub fn new(
        board_receiver: watch::Receiver<Board>,
        render_config: RenderConfig,
        player_request_receiver: Option<mpsc::Receiver<PlayerRequest>>,
    ) -> (Self, watch::Receiver<MouseInformation>) {
        let svg_strings = render_config.get_svg_strings();
        let textures = Self::generate_textures(&svg_strings);
        let (sender, receiver) = watch::channel(MouseInformation {
            mouse_pos_on_board: MousePosOnBoard::MouseOffOfBoard,
            left_mouse_pressed: false,
            left_mouse_pressed_last_frame: false,
        });

        (
            Self {
                config: render_config,
                svg_strings: svg_strings,
                board_receiver: board_receiver,
                info_sender: sender,
                textures: textures,
                prev_square_size: Self::get_square_size(),
                player_request_receiver,
                left_mouse_pressed_last_frame: false,
            },
            receiver,
        )
    }

    pub fn render(&mut self) {
        let min_square_size = Self::get_square_size();
        self.render_game();
        self.prev_square_size = min_square_size;
    }

    fn render_game(&mut self) {
        clear_background(self.config.background_colour());

        let square_size = Self::get_square_size();
        for file in position::File::iter() {
            for rank in position::Rank::iter().rev() {
                self.render_square(rank, file, square_size);
            }
        }

        self.info_sender.send(MouseInformation {
            mouse_pos_on_board: self.get_mouse_pos_on_board(),
            left_mouse_pressed_last_frame: self.left_mouse_pressed_last_frame,
            left_mouse_pressed: is_mouse_button_down(MouseButton::Left),
        });

        self.left_mouse_pressed_last_frame = is_mouse_button_down(MouseButton::Left);
    }

    fn get_square_size() -> f32 {
        let min_screen_size = f32::min(screen_height(), screen_width());
        min_screen_size / (Board::SIZE as f32)
    }

    fn render_square(&mut self, rank: Rank, file: File, square_size: f32) {
        let x = file.clone() as u8;
        let y = rank.clone() as u8;

        let colour = if (x + y) % 2 == 0 {
            self.config.black_squares_colour()
        } else {
            self.config.white_squares_colour()
        };

        let x_offset = (screen_width() - square_size * (Board::SIZE as f32)) / 2.0;
        let y_offset = (screen_height() - square_size * (Board::SIZE as f32)) / 2.0;

        let x_total = (x as f32) * square_size + x_offset;
        let y_total = (y as f32) * square_size + y_offset;

        draw_rectangle(x_total, y_total, square_size, square_size, colour);

        let square = self
            .board_receiver
            .borrow()
            .get(&Position::encode(rank, file).clone());

        if let Square::Occupied(piece) = square {
            let texture = self.textures().get(&piece);
            draw_texture(texture.clone(), x_total, y_total, WHITE);
        }
    }

    fn svg_to_texture(svg_str: &str, size: u32) -> Texture2D {
        let opt = resvg::usvg::Options::default();
        let tree = resvg::usvg::Tree::from_str(svg_str, &opt).unwrap();
        let mut pixmap = resvg::tiny_skia::Pixmap::new(size, size).unwrap();

        resvg::render(
            &tree,
            resvg::usvg::FitTo::Height(size),
            resvg::tiny_skia::Transform::default(),
            pixmap.as_mut(),
        )
        .unwrap();
        let png = pixmap.encode_png().unwrap();
        Texture2D::from_file_with_format(&png, Some(ImageFormat::Png))
    }

    fn get_mouse_pos_on_board(&self) -> MousePosOnBoard {
        let mouse_pos = mouse_position_local();

        let rank_scale = screen_width() / (Self::get_square_size() * (Board::SIZE as f32));
        let file_scale = screen_height() / (Self::get_square_size() * (Board::SIZE as f32));

        let y: i16 = (4.0 * (mouse_pos.y * file_scale + 1.0)).floor() as i16;
        let x: i16 = (4.0 * (mouse_pos.x * rank_scale + 1.0)).floor() as i16;

        let file: File;
        let rank: Rank;

        if x < 0 || y < 0 {
            return MousePosOnBoard::MouseOffOfBoard;
        }

        if let Some(f) = File::from_repr(x as usize) {
            file = f;
        } else {
            return MousePosOnBoard::MouseOffOfBoard;
        }
        if let Some(r) = Rank::from_repr(y as usize) {
            rank = r;
        } else {
            return MousePosOnBoard::MouseOffOfBoard;
        }

        return MousePosOnBoard::MouseOnBoard(Position::encode(rank, file));
    }
}
