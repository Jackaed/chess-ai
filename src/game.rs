use crate::{board, board::Board, colour::Colour, player::Player};

#[derive(Clone)]
pub struct Game {
    pub board: Board,
    white: Player,
    black: Player,
}

impl Game {
    /// Creates a new game from the starting position
    pub fn new(white: Player, black: Player) -> Self {
        Self {
            board: Board::create_starting_board(),
            white,
            black,
        }
    }

    /// Plays the game, until the game has a winner, at which point it returns.
    pub fn play(&mut self) -> board::Winner {
        let mut winner_opt: Option<board::Winner>;
        loop {
            winner_opt = self.board.winner();
            if let Some(winner) = winner_opt {
                return winner;
            }
            self.board
                .move_piece(
                    (match self.board.current_turn() {
                        Colour::White => &mut self.white,
                        Colour::Black => &mut self.black,
                    })
                    .suggest_move(&self.board),
                )
                .unwrap_or_else(|move_err| print!("Illegal move: {0}", move_err.reason));
        }
    }

    pub fn board(&self) -> &Board {
        &self.board
    }
}
