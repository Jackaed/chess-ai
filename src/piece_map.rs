use enum_map::{enum_map, EnumMap};

use crate::{colour::Colour, piece, piece::Piece};

#[derive(Clone)]
pub struct PieceMap<T: Clone> {
    map: EnumMap<Colour, EnumMap<piece::Kind, T>>,
}

impl<T: Clone> PieceMap<T> {
    pub fn get(&self, piece: &Piece) -> &T {
        &self.map[piece.colour][piece.kind]
    }

    pub fn get_mut(&mut self, piece: &Piece) -> &mut T {
        &mut self.map[piece.colour][piece.kind]
    }

    pub fn set(&mut self, piece: &Piece, value: T) {
        self.map[piece.colour][piece.kind] = value;
    }

    pub fn new<F>(created_value: F) -> Self
    where
        F: Fn(Piece) -> T,
    {
        PieceMap {
            map: enum_map! {
                colour => enum_map! {
                    kind => {
                        created_value(Piece::new(kind, colour))
                    }
                }
            },
        }
    }
}
