use std::collections::hash_map::RandomState;

use crate::{
    bitboard::Bitboard,
    position::{File, Position, Rank},
};

fn main() {
    const MAGIC_SIZE: usize = 12;
    let mut table = [[0; 2 ^ MAGIC_SIZE]; 64];
}

struct TableEntry {
    magic_number: u64,
    bits_per_entry: u8,
    magic_table: Vec<u64>,
}

pub fn gen_attack_mask_rook(rook_position: Position, blocker_mask: u64) -> u64 {
    let blocker_bitboard = Bitboard::from_data(blocker_mask);
    let mut attack_bitboard = Bitboard::from_data(0);

    gen_attack_mask_in_dir(rook_position, blocker_bitboard, 1, 0, &mut attack_bitboard);
    gen_attack_mask_in_dir(rook_position, blocker_bitboard, -1, 0, &mut attack_bitboard);
    gen_attack_mask_in_dir(rook_position, blocker_bitboard, 0, 1, &mut attack_bitboard);
    gen_attack_mask_in_dir(rook_position, blocker_bitboard, 0, -1, &mut attack_bitboard);

    return attack_bitboard.data();
}

pub fn gen_attack_mask_bishop(bishop_position: Position, blocker_mask: u64) -> u64 {
    let blocker_bitboard = Bitboard::from_data(blocker_mask);
    let mut attack_bitboard = Bitboard::from_data(0);

    gen_attack_mask_in_dir(
        bishop_position,
        blocker_bitboard,
        1,
        1,
        &mut attack_bitboard,
    );
    gen_attack_mask_in_dir(
        bishop_position,
        blocker_bitboard,
        -1,
        -1,
        &mut attack_bitboard,
    );
    gen_attack_mask_in_dir(
        bishop_position,
        blocker_bitboard,
        -1,
        1,
        &mut attack_bitboard,
    );
    gen_attack_mask_in_dir(
        bishop_position,
        blocker_bitboard,
        1,
        -1,
        &mut attack_bitboard,
    );

    return attack_bitboard.data();
}

fn gen_attack_mask_in_dir(
    position: Position,
    blocker_bitboard: Bitboard,
    x_offset: i8,
    y_offset: i8,
    attack_bitboard: &mut Bitboard,
) {
    let mut square = position;
    loop {
        let Some(rank) = Rank::from_repr((square.rank() as i8 + x_offset) as usize) else {
            break;
        };
        let Some(file) = File::from_repr((square.file() as i8 + y_offset) as usize) else {
            break;
        };
        square = Position::encode(rank, file);
        attack_bitboard.set(&square);
        if blocker_bitboard.get(&square) {
            break;
        }
    }
}

fn gen_magic_for_rook_on_square(position: &Position) {
    let magic: u64 = 0;
    let mut blocker_board: Bitboard = Bitboard::new();
    let mut blocker_int: u32 = 0;
    let mut blocker_int_shared: u32;
    let mut table: Vec<u64>;

    // Rook guarantees that there are 2^14 valid combinations of blockers for each square.
    while blocker_int < 16384 {
        blocker_int_shared = blocker_int;

        blocker_int_shared = gen_blocker_board_in_dir(position, 1, 0, &mut blocker_board, blocker_int_shared);
        blocker_int_shared = gen_blocker_board_in_dir(position, 0, 1, &mut blocker_board, blocker_int_shared);
        blocker_int_shared = gen_blocker_board_in_dir(position, 0, -1, &mut blocker_board, blocker_int_shared);
        gen_blocker_board_in_dir(position, -1, 0, &mut blocker_board, blocker_int_shared);

        let index: usize = (blocker_board.data() * magic) as usize;


        blocker_int += 1;
    }
}

fn gen_blocker_board_in_dir(
    position: &Position,
    x_offset: i8,
    y_offset: i8,
    blocker_board: &mut Bitboard,
    mut blocker_int: u32,
) -> u32 {
    loop {
        let Some(rank) = Rank::from_repr((position.rank() as i8 + x_offset) as usize) else {
            break;
        };
        let Some(file) = File::from_repr((position.file() as i8 + y_offset) as usize) else {
            break;
        };

        if (blocker_int & 1) == 1 {
            blocker_board.set(&Position::encode(rank, file));
        }
        blocker_int = blocker_int >> 1;
    }

    return blocker_int;
}
