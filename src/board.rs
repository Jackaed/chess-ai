use std::{fmt::Display, str::FromStr};

use enum_map::*;
use strum::IntoEnumIterator;
use strum_macros::Display;

use crate::{
    bitboard,
    colour::Colour,
    errors::{InvalidChar, InvalidFenString, InvalidMove},
    p_move,
    p_move::Move,
    piece::Piece,
    position,
    position::Position,
};

#[derive(Display)]
pub enum Winner {
    Stalemate,
    Won(Colour),
}

#[derive(Display, Clone, Copy)]
pub enum Square {
    Occupied(Piece),
    Empty,
}

#[derive(Clone)]
pub struct Board {
    data: bitboard::Map,
    en_passant_target: Option<Position>,
    current_turn: Colour,
    half_move_clock: u32,
    full_move_clock: u32,
    castling_rights: CastlingRights,
}

impl Board {
    pub fn empty() -> Board {
        Board {
            data: bitboard::Map::new(),
            en_passant_target: None,
            current_turn: Colour::White,
            full_move_clock: 1,
            half_move_clock: 0,
            castling_rights: CastlingRights::new(),
        }
    }

    pub fn winner(&self) -> Option<Winner> {
        return None;
    }

    pub fn current_turn(&self) -> &Colour {
        &self.current_turn
    }

    pub const SIZE: usize = 8;

    pub fn get(&self, position: &Position) -> Square {
        self.data.get_piece(position)
    }

    pub fn set(&mut self, position: &Position, piece: Piece) {
        self.data.set_piece(position, piece);
    }

    pub fn remove(&mut self, position: &Position) {
        self.data.remove_piece(position)
    }

    pub fn move_piece(&mut self, piece_move: Move) -> Result<(), InvalidMove> {
        println!("{}", piece_move);
        if let Square::Occupied(mut piece_from) = self.get(&piece_move.from) {
            self.process_move_flag(piece_move, &mut piece_from)?;
            self.set(&piece_move.to, piece_from);
            self.remove(&piece_move.from);
            self.current_turn = self.current_turn.other();
            Ok(())
        } else {
            Err(InvalidMove {
                reason: "No piece found at position \"from\" to be moved.".into(),
            })
        }
    }

    fn process_move_flag(
        &mut self,
        piece_move: Move,
        piece_from: &mut Piece,
    ) -> Result<(), InvalidMove> {
        Ok(match piece_move.flag {
            p_move::Flag::Regular => (),
            p_move::Flag::Promote(kind) => {
                *piece_from = Piece {
                    kind: kind,
                    colour: piece_from.colour,
                };
            }
            p_move::Flag::Castle => {
                self.castle(&piece_move)?;
            }
            p_move::Flag::PawnDoubleMove => todo!(),
            p_move::Flag::EnPassant => todo!(),
        })
    }

    fn castle(&mut self, piece_move: &Move) -> Result<(), InvalidMove> {
        let rook_from_position;
        let rook_is_castling_right: bool = piece_move.from.file() > piece_move.to.file();
        let rook_to_position: Position = {
            rook_from_position = Position::encode(
                piece_move.from.rank(),
                if rook_is_castling_right {
                    position::File::A
                } else {
                    position::File::H
                },
            );
            (if rook_is_castling_right {
                piece_move.to.add(&Position::A2)
            } else {
                piece_move.to.sub(&Position::A2)
            })
            .map_err(|_| InvalidMove {
                reason: "Move attempts to castle rook off the board".into(),
            })?
        };
        self.castle_rook(&rook_from_position, &rook_to_position)?;
        Ok(())
    }

    fn castle_rook(&mut self, from: &Position, to: &Position) -> Result<(), InvalidMove> {
        if let Square::Occupied(piece) = self.get(from) {
            self.set(to, piece.clone());
            self.remove(&from);
            return Ok(());
        }
        Err(InvalidMove {
            reason: "Empty piece where rook is supposed to be when castling.".into(),
        })
    }

    pub fn create_starting_board() -> Board {
        // cspell:disable-next-line
        Self::from_fen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1").unwrap()
    }

    /// Gets a Board from a given [FEN
    /// string](https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation)
    pub fn from_fen(fen_string: &str) -> Result<Board, InvalidFenString> {
        let mut rank_iter = position::Rank::iter();
        let mut file_iter = position::File::iter();
        let mut rank = rank_iter.next();
        let mut file = file_iter.next();
        let mut field_num: u8 = 0;
        let mut board: Board = Board::empty();
        board.castling_rights = CastlingRights {
            data: enum_map! { _ => enum_map! {_ => false} },
        };

        let mut fen_string_iter = fen_string.chars();
        while let Some(character) = fen_string_iter.next() {
            if character == ' ' {
                field_num += 1;
                continue;
            }

            if field_num == 0 {
                Self::parse_char_in_first_fen_field(
                    character,
                    &mut rank,
                    &mut rank_iter,
                    &mut file_iter,
                    &mut file,
                    &mut board,
                )?;
            }

            if field_num == 1 {
                board.current_turn =
                    Colour::try_from(character).map_err(|_| InvalidFenString {})?;
            }

            if field_num == 2 {
                if character == '-' {
                    continue;
                }

                let colour = if character.is_lowercase() {
                    Colour::White
                } else {
                    Colour::Black
                };

                let board_side_opt = BoardSide::try_from(character);

                if let Ok(board_side) = board_side_opt {
                    board.castling_rights.data[colour][board_side] = true;
                }
            }

            if field_num == 3 {
                if character == '-' {
                    continue;
                }

                board.en_passant_target = 
                Some(
                    Position::try_from([
                        character,
                        fen_string_iter.next().ok_or_else(|| InvalidFenString {})?,
                    ])
                    .map_err(|_| InvalidFenString {})?,
                );
            }

            if field_num == 4 || field_num == 5 {
                let val = character.to_digit(10).ok_or(InvalidFenString {})?;

                if field_num == 4 {
                    board.half_move_clock = val;
                    board.half_move_clock = val;
                } else if field_num == 5 {
                    board.full_move_clock = val;
                    board.full_move_clock = val;
                }
            }
        }

        Ok(board)
    }

    fn parse_char_in_first_fen_field(
        character: char,
        rank: &mut Option<position::Rank>,
        rank_iter: &mut position::RankIter,
        file_iter: &mut position::FileIter,
        file: &mut Option<position::File>,
        board: &mut Board,
    ) -> Result<(), InvalidFenString> {
        Ok(if character == '/' {
            *rank = rank_iter.next();
            *file_iter = position::File::iter();
            *file = file_iter.next();
        } else if let Some(digit) = character.to_digit(10) {
            for _ in 0..digit {
                *file = file_iter.next();
            }
        } else {
            if let Some(rank) = *rank {
                if let Some(file) = *file {
                    board.set(
                        &Position::encode(rank, file),
                        Piece::try_from(character).map_err(|_| InvalidFenString {})?,
                    );
                }
            }
            *file = file_iter.next();
        })
    }
}

impl FromStr for Board {
    type Err = InvalidFenString;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::from_fen(s)
    }
}

impl Display for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut out = "".to_owned();
        for rank in position::Rank::iter() {
            for file in position::File::iter() {
                let square = self.get(&Position::encode(rank, file));
                out = format!(
                    "{}{}",
                    out,
                    if let Square::Occupied(piece) = square {
                        piece.to_string().to_owned()
                    } else {
                        "-".to_owned()
                    }
                );
                out += " ";
            }
            out += "\n";
        }
        out += "\n";

        write!(f, "{}", out)
    }
}

#[derive(Enum, Clone, Copy)]
enum BoardSide {
    QueenSide,
    KingSide,
}

impl TryFrom<char> for BoardSide {
    type Error = InvalidChar;
    fn try_from(character: char) -> Result<Self, Self::Error> {
        match character {
            'K' | 'k' => Ok(BoardSide::KingSide),
            'Q' | 'q' => Ok(BoardSide::QueenSide),
            _ => Err(InvalidChar {}),
        }
    }
}

/// Used to store who can legally castle in a given position in a board.
#[derive(Clone, Copy)]
struct CastlingRights {
    data: EnumMap<Colour, EnumMap<BoardSide, bool>>,
}

impl CastlingRights {
    pub fn new() -> CastlingRights {
        CastlingRights {
            data: enum_map! { _ => enum_map! { _ => true} },
        }
    }

    /// Gets the castling rights for a given side of the board and colour,
    /// returning true if castling is possible, or false if not.
    pub fn get(&self, colour: &Colour, side: &BoardSide) -> &bool {
        &self.data[*colour][*side]
    }

    /// Removes castling rights for a given side of the board and a given
    /// colour.
    pub fn remove(&mut self, colour: &Colour, side: &BoardSide) {
        self.data[*colour][*side] = false;
    }
}
