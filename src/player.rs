use std::io::stdin;

use crate::{
    board::Board,
    p_move::{Flag, Move},
    position::Position,
};

#[derive(Clone)]
pub enum Player {
    Person,
}

impl Player {
    pub fn suggest_move(&mut self, board: &Board) -> Move {
        match self {
            Player::Person => person_suggest_move(board),
        }
    }
}

fn person_suggest_move(board: &Board) -> Move {
    println!("{}", board);

    let from = get_pos_from_terminal("Input the position from which the piece will be moved: ");
    let to = get_pos_from_terminal("Input a position to move the piece to: ");

    return Move {
        from,
        to,
        flag: Flag::Regular,
    };
}

fn get_pos_from_terminal(message: &str) -> Position {
    loop {
        println!("{}", message);
        let mut buffer = String::new();

        let Ok(_) = stdin().read_line(&mut buffer) else {
            continue;
        };

        let Some(x) = buffer.chars().nth(0) else {
            continue;
        };
        let Some(y) = buffer.chars().nth(1) else {
            continue;
        };
        let Ok(pos) = Position::try_from([x, y]) else {
            continue;
        };
        break pos;
    }
}
