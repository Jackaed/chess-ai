use std::fmt::Display;

use strum_macros::Display;

use crate::{piece, position::Position};

#[derive(Clone, Copy, PartialEq)]
pub struct Move {
    pub from: Position,
    pub to: Position,
    pub flag: Flag,
}

impl Display for Move {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "From: {}, To: {}, Flag: {}",
            self.from, self.to, self.flag
        )
    }
}

#[derive(Clone, Copy, PartialEq, Display)]
pub enum Flag {
    Regular,
    Promote(piece::Kind),
    PawnDoubleMove,
    EnPassant,
    Castle,
}
