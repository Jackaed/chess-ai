use enum_map::*;

use crate::{board, piece::Piece, piece_map::PieceMap, position::Position};

#[derive(Clone, Copy)]
pub struct Bitboard {
    data: u64,
}

impl Default for Bitboard {
    fn default() -> Self {
        Self::new()
    }
}

impl Bitboard {
    pub fn new() -> Bitboard {
        Bitboard { data: 0 }
    }

    pub fn get(&self, position: &Position) -> bool {
        (self.data & (1 << (*position as usize))) != 0
    }

    pub fn set(&mut self, position: &Position) {
        self.data |= 1 << (*position as usize);
    }

    pub fn remove(&mut self, position: &Position) {
        self.data &= !(1 << (*position as usize));
    }

    pub fn from_data (data: u64) -> Self {
        Self { data }
    }

    pub fn data(&self) -> u64 {
        self.data
    }
}

#[derive(Clone)]
pub struct Map {
    data: PieceMap<Bitboard>,
}

impl Map {
    pub fn new() -> Map {
        Map {
            data: PieceMap::new(|_| Bitboard::default()),
        }
    }

    fn get_bitboard_mut(&mut self, piece: &Piece) -> &mut Bitboard {
        self.data.get_mut(piece)
    }

    fn get_bitboard(&self, piece: &Piece) -> &Bitboard {
        self.data.get(piece)
    }

    pub fn get_piece(&self, position: &Position) -> board::Square {
        for piece in Piece::iter() {
            if self.get_bitboard(&piece).get(position) {
                return board::Square::Occupied(piece);
            }
        }
        board::Square::Empty
    }

    pub fn set_piece(&mut self, position: &Position, piece: Piece) {
        self.remove_piece(position);
        self.get_bitboard_mut(&piece).set(position);
    }

    pub fn remove_piece(&mut self, position: &Position) {
        for piece in Piece::iter() {
            let bitboard = self.get_bitboard_mut(&piece);
            if bitboard.get(position) {
                bitboard.remove(position);
                return;
            }
        }
    }
}
