use std::str::FromStr;

use strum_macros::*;

use crate::errors::{InvalidChar, PositionOutOfBounds};

// Skipping rust format on the position enum to allow the nice formatting
#[rustfmt::skip]
#[derive(Display, EnumString, EnumIter, FromRepr, Clone, Copy, PartialEq)]
pub enum Position {
    A8, B8, C8, D8, E8, F8, G8, H8,
    A7, B7, C7, D7, E7, F7, G7, H7,
    A6, B6, C6, D6, E6, F6, G6, H6,
    A5, B5, C5, D5, E5, F5, G5, H5,
    A4, B4, C4, D4, E4, F4, G4, H4,
    A3, B3, C3, D3, E3, F3, G3, H3,
    A2, B2, C2, D2, E2, F2, G2, H2,
    A1, B1, C1, D1, E1, F1, G1, H1,
}

#[derive(Debug)]
pub struct InvalidPositionError;

impl TryFrom<[char; 2]> for Position {
    type Error = InvalidChar;
    fn try_from(chars: [char; 2]) -> Result<Self, Self::Error> {
        let mut str: String = chars.iter().collect();
        str = str.to_ascii_uppercase();
        Position::from_str(&str).map_err(|_| InvalidChar {})
    }
}

impl Position {
    pub fn rank(&self) -> Rank {
        Rank::from_repr((self.clone() as usize) / 8).unwrap()
    }

    pub fn file(&self) -> File {
        File::from_repr((self.clone() as usize) % 8).unwrap()
    }

    pub fn encode(rank: Rank, file: File) -> Self {
        Self::from_repr((rank as usize) * 8 + (file as usize))
            .expect("Invalid rank and file conversion to position")
    }

    pub fn add(&self, other: &Self) -> Result<Self, PositionOutOfBounds> {
        Self::from_repr((*self as usize) + (*other as usize)).ok_or(PositionOutOfBounds {})
    }

    pub fn sub(&self, other: &Self) -> Result<Self, PositionOutOfBounds> {
        Self::from_repr((*self as usize) - (*other as usize)).ok_or(PositionOutOfBounds {})
    }
}

#[derive(FromRepr, EnumIter, Clone, Copy)]
pub enum Rank {
    R8,
    R7,
    R6,
    R5,
    R4,
    R3,
    R1,
    R2,
}

#[derive(FromRepr, PartialOrd, PartialEq, EnumIter, Clone, Copy)]
pub enum File {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
}
